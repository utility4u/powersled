export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Powersled',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/png', href: '/favicon.png' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main.scss',
    '@/assets/css/normalize.css',
    '@/assets/font/css/clash-grotesk.css',
  ],

  styleResources: {
    scss: ['./assets/css/*.scss']
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/style-resources',
    'nuxt-i18n',
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  i18n: {
    locales: ['en', 'cs'],
    defaultLocale: 'en',
    strategy: 'prefix_except_default',
    vueI18n: {
      fallbackLocale: 'en',
      messages: {
        en: {
          product: 'Product',
          videos: 'Videos',
          aboutUs: 'About us',
          players: 'Players',
          contact: 'Contact',
          sentence0: 'Foldable and patented design ',
          sentence1: 'fits all hockey rinks.',
          title0: 'The future ',
          title1: 'of on - ice ',
          title2: 'strength ',
          title3: '& ',
          title4: 'conditioning ',
          productTitle: 'Designed for power',
          product0: 'On ice assembly takes only 10 seconds.',
          product1: 'No screws or tools required.',
          product2: 'Unbreakable design.',
          product3: 'Can be used off-ice as well.',
          product4: 'Storable at any hockey bench.',
          product5: 'Corner safety bumpers included.',
          product6: 'Create your own team’s colors & design.',
          product7: '100% money back guarantee.',
          product8: 'Add regular size weight plates as required.',
          product9: 'Made in Europe - 100% stainless steel.',
          product10: 'Lifetime Warranty.',
          aboutText0: 'Designed by Ales Parez, European ',
          aboutText1: 'strength & conditioning coach ',
          aboutText2: 'of several NHL players and ',
          aboutText3: 'Stanley Cups winners.',
          aboutDescription0: 'Our mission was to create a solid strength & conditioning tool ',
          aboutDescription1: 'for all hockey players who want to stay on the ice after practice and work on their leg power and lung capacity. Sled concept is not new in the gym but on the ice… it’s a game changer.',
          testimonial: '„Amazing strength & conditioning tool for all hockey players who want to stay on the ice after and work on their leg power and lung capacity. „',
          sendMessage: 'Send message',
          contactUs: 'Contact us',
          name: 'Name',
          email: 'Email',
          phoneNumber: 'Phone number',
          message: 'Message',
        },
        cs: {
          product: 'Produkt',
          videos: 'Video',
          aboutUs: 'O nás',
          players: 'Hráči',
          contact: 'Kontakt',
          sentence0: 'Patentovaný rozkládací design ',
          sentence1: 'vhodný pro všechna kluziště.',
          title0: 'Budoucnost ',
          title1: 'hokejové ',
          title2: 'silové ',
          title3: '& ',
          title4: 'kondiční přípravy',
          productTitle: 'Vyrobeno pro vítěze',
          product0: 'Složení zabere pouhých 10 sekund.',
          product1: 'Není třeba žádné nářadí.',
          product2: 'Nezničitelný design.',
          product3: 'Lze použít i mimo led.',
          product4: 'Skladovatelné na jakékoli střídačce.',
          product5: 'Gumové ochrany na rozích pro vyšší bezpečnost.',
          product6: 'Navrhněte si design ve vašich klubových barvách',
          product7: '100% záruka vrácení peněz.',
          product8: 'Lze přidat závaží.',
          product9: 'Vyrobeno v Evropě ze 100% nerezové oceli.',
          product10: 'Doživotní záruka.',
          aboutText0: 'Navrženo Alešem Pařezem, ',
          aboutText1: 'silovým a kondičním trenérem ',
          aboutText2: 'řady hráčů NHL a ',
          aboutText3: 'vítězů Stanley Cupu.',
          aboutDescription0: 'Naším cílem bylo vytvořit silové a kondiční náčiní ',
          aboutDescription1: 'vhodné pro všechny hokejisty, kteří se nebojí přidat si po týmovém tréninku a pracovat na své síle a kapacitě plic. Koncept saní je stálicí v mnoha posilovnách po celém světě, ale na ledě je průlomovou novinkou!',
          testimonial: '„Úžasný silový a kondiční nástroj pro všechny hokejisty, kteří chtějí zůstat na ledě a pracovat na síle nohou a kapacitě plic. „',
          sendMessage: 'Poslat zprávu',
          contactUs: 'Kontakty',
          name: 'Jméno',
          email: 'E-mail',
          phoneNumber: 'Telefonní číslo',
          message: 'Zpráva',
        },
      }
    }
  }
}
